<?php
namespace Clex\V1\Rest\Merchant;

class MerchantResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Clex\V1\Rest\Merchant\MerchantMapper');
        return new MerchantResource($mapper);
    }
}

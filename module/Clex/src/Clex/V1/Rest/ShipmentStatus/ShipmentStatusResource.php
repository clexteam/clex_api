<?php
namespace Clex\V1\Rest\ShipmentStatus;

use Herrera\Json\Exception\Exception;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ShipmentStatusResource extends AbstractResourceListener
{
    protected $mapper;
    public $merchantId;
    public $password;

    public function __construct($mapper)
    {
        $this->mapper = $mapper;
        $this->merchantId = @trim($_GET['merchantId']);
        $this->password = @trim($_GET['password']);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        //-- authorization
        try{
            $this->mapper->authenticate($this->merchantId, $this->password);
        }
        catch(Exception $e)
        {
            return new ApiProblem(401, 'unauthorized access');
        }

        try{
            $entity = $this->mapper->create($data);
            return $entity;
        }
        catch(Exception $e){
            return new ApiProblem(500, 'Error inserting data!');
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        //-- authorization
        try{
            $this->mapper->authenticate($this->merchantId, $this->password);
        }
        catch(Exception $e)
        {
            return new ApiProblem(401, 'unauthorized access');
        }

        return $this->mapper->fetch($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        //-- authorization
        try{
            $this->mapper->authenticate($this->merchantId, $this->password);
        }
        catch(Exception $e)
        {
            return new ApiProblem(401, 'unauthorized access');
        }

        return $this->mapper->fetchAll($params);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

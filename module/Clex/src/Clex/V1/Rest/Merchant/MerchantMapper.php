<?php
/**
 * Created by PhpStorm.
 * User: Remon Adel
 * Date: 1/20/2017
 * Time: 12:21 AM
 */

namespace Clex\V1\Rest\Merchant;

use Herrera\Json\Exception\Exception;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;

class MerchantMapper
{
    protected $adapter;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function fetchAll($params = [])
    {
        $select = new Select('CL_BRANCHES');
        $select->where("CUST_ACC_MAIN = '".$params['main_client']."'");
        $select->order('Branch_ID desc');
        $paginatorAdapter = new DbSelect($select, $this->adapter);
        $collection = new MerchantCollection($paginatorAdapter);
        return $collection;
    }

    public function create($data)
    {
        if(!$data->merchantId)
        {
            $sql = "INSERT INTO CL_BRANCHES (CUST_ACC, SH_COMPANY, SH_DISTRICT, STREET, SH_CITY, SH_ZIP, SH_CONTACT,
                  SH_TEL, SH_MOB, email, ACTIVE, CUST_ACC_MAIN)
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try{
                $resultset = $this->adapter->query($sql, array($data->merchantName, $data->companyName, $data->district, $data->street,
                    $data->city, $data->zip, $data->contact, $data->telephone, $data->mobile, $data->email, 1, $data->mainCompanyName));
            }catch (Exception $e){
                echo $e->getMessage();
            }

            $insertedId = $this->adapter->driver->getLastGeneratedValue();

            $sql = "INSERT INTO SEC_USER (CUST_ACC, Branch_ID, USER_NAME, USER_PWD)
                    VALUES (?, ?, ?, ?)";

            $merchantPassword = $this->randomPassword();
            $resultset = $this->adapter->query($sql, array($data->mainCompanyName, $insertedId, $data->merchantName, $merchantPassword));

            $this->adapter->driver->getLastGeneratedValue();

            $hydratedData = array('Branch_ID' => $insertedId, 'CUST_ACC' => $data->merchantName, 'SH_COMPANY' => $data->companyName, 'SH_DISTRICT' => $data->district,
                'STREET' => $data->street, 'SH_CITY' => $data->city, 'SH_ZIP' => $data->zip, 'SH_CONTACT' => $data->contact, 'SH_TEL' =>$data->telephone,
                'SH_MOB' =>  $data->mobile, 'email' => $data->email, 'CUST_ACC_MAIN' => $data->mainCompanyName, 'password' => $merchantPassword);

            $entity = new MerchantEntity();
            $entity->exchangeArray($hydratedData);

            return $entity;
        }
        else
        {
            $sql = "UPDATE CL_BRANCHES SET CUST_ACC = ?, SH_COMPANY = ?, SH_DISTRICT = ?, STREET = ?, SH_CITY = ?, SH_ZIP = ?,
                  SH_CONTACT = ?, SH_TEL = ?, SH_MOB = ?, email = ?, ACTIVE = ?, CUST_ACC_MAIN = ? WHERE Branch_ID = ?";
            try{
                $resultset = $this->adapter->query($sql, array($data->merchantName, $data->companyName, $data->district, $data->street,
                    $data->city, $data->zip, $data->contact, $data->telephone, $data->mobile, $data->email, 1, $data->mainCompanyName, $data->merchantId));
            }catch (Exception $e){
                return $e->getMessage();
            }
            $hydratedData = array('Branch_ID' => $data->merchantId, 'CUST_ACC' => $data->merchantName, 'SH_COMPANY' => $data->companyName, 'SH_DISTRICT' => $data->district,
                'STREET' => $data->street, 'SH_CITY' => $data->city, 'SH_ZIP' => $data->zip, 'SH_CONTACT' => $data->contact, 'SH_TEL' =>$data->telephone,
                'SH_MOB' =>  $data->mobile, 'email' => $data->email, 'CUST_ACC_MAIN' => $data->mainCompanyName);

            $entity = new MerchantEntity();
            $entity->exchangeArray($hydratedData);

            return $entity;
        }

    }

    private function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function fetch($id)
    {
        $select = new Select('CL_BRANCHES');
        $paginatorAdapter = new DbSelect($select, $this->adapter);
        $collection = new MerchantCollection($paginatorAdapter);
        return $collection;
    }

}
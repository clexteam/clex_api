<?php
namespace Clex\V1\Rest\ShipmentStatus;

class ShipmentStatusEntity
{
    public $shipmentCode;
    public $status;
    public $statusTitle;
    public $statusDetails;
    public $statusNotes;

    public function getArrayCopy()
    {
        return array(
            'shipmentCode' => $this->shipmentCode,
            'status' => $this->status,
            'statusTitle' => $this->statusTitle,
            'statusDetails' => $this->statusDetails,
            'statusNotes' => $this->statusNotes
        );
    }

    public function exchangeArray(array $array)
    {
        $this->shipmentCode = $array['SIHPMENT_CODE'];
        $this->status = $array['SHIPMENT_STATUS'];
        $this->statusNotes  = $array['STATUS_NOTES'];
        $this->statusTitle  = $array['title'];
        $this->statusDetails  = $array['details'];
    }
}

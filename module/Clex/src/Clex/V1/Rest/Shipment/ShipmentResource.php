<?php
namespace Clex\V1\Rest\Shipment;

use Herrera\Json\Exception\Exception;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ShipmentResource extends AbstractResourceListener
{
    protected $mapper;
    public $merchantId;
    public $password;

    public function __construct($mapper)
    {
        $this->mapper = $mapper;
        $this->merchantId = @trim($_GET['merchantId']);
        $this->password = @trim($_GET['password']);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        //-- authorization
        try{
            $merchantInfo = $this->mapper->authenticate($this->merchantId, $this->password);
        }
        catch(Exception $e)
        {
            return new ApiProblem(401, 'unauthorized access');
        }

        try{
            $data->customerAccount = $merchantInfo[0]['CUST_ACC'];
            $data->userId = $merchantInfo[0]['USER_ID'];
            $entity = $this->mapper->create($data);
            return $entity;
        }
        catch(Exception $e){
            return new ApiProblem(500, 'Error inserting or updating data!');
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try{
            $merchantInfo = $this->mapper->authenticate($this->merchantId, $this->password);
        }
        catch(Exception $e)
        {
            return new ApiProblem(401, 'unauthorized access');
        }

        try{
            return $this->mapper->fetch($id);
        }
        catch(Exception $e){
            return new ApiProblem(500, 'Error inserting or updating data!');
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

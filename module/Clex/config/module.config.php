<?php
return array(
    'router' => array(
        'routes' => array(
            'clex.rest.merchant' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/merchant[/:merchant_id]',
                    'constraints' => array(
                        'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Clex\\V1\\Rest\\Merchant\\Controller',
                    ),
                ),
            ),
            'clex.rest.shipment' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/shipment[/:shipment_id]',
                    'defaults' => array(
                        'controller' => 'Clex\\V1\\Rest\\Shipment\\Controller',
                    ),
                ),
            ),
            'clex.rest.shipment-status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/shipment-status[/:shipment_status_id]',
                    'defaults' => array(
                        'controller' => 'Clex\\V1\\Rest\\ShipmentStatus\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'clex.rest.merchant',
            1 => 'clex.rest.shipment',
            2 => 'clex.rest.shipment-status',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Clex\\V1\\Rest\\Merchant\\MerchantResource' => 'Clex\\V1\\Rest\\Merchant\\MerchantResourceFactory',
            'Clex\\V1\\Rest\\Shipment\\ShipmentResource' => 'Clex\\V1\\Rest\\Shipment\\ShipmentResourceFactory',
            'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusResource' => 'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'Clex\\V1\\Rest\\Merchant\\Controller' => array(
            'listener' => 'Clex\\V1\\Rest\\Merchant\\MerchantResource',
            'route_name' => 'clex.rest.merchant',
            'route_identifier_name' => 'merchant_id',
            'collection_name' => 'merchant',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'main_client',
                1 => 'merchantId',
                2 => 'password'
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Clex\\V1\\Rest\\Merchant\\MerchantEntity',
            'collection_class' => 'Clex\\V1\\Rest\\Merchant\\MerchantCollection',
            'service_name' => 'Merchant',
        ),
        'Clex\\V1\\Rest\\Shipment\\Controller' => array(
            'listener' => 'Clex\\V1\\Rest\\Shipment\\ShipmentResource',
            'route_name' => 'clex.rest.shipment',
            'route_identifier_name' => 'shipment_id',
            'collection_name' => 'shipment',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'merchantId',
                1 => 'password'
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Clex\\V1\\Rest\\Shipment\\ShipmentEntity',
            'collection_class' => 'Clex\\V1\\Rest\\Shipment\\ShipmentCollection',
            'service_name' => 'shipment',
        ),
        'Clex\\V1\\Rest\\ShipmentStatus\\Controller' => array(
            'listener' => 'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusResource',
            'route_name' => 'clex.rest.shipment-status',
            'route_identifier_name' => 'shipment_status_id',
            'collection_name' => 'shipment_status',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'merchant_id',
                1 => 'merchantId',
                2 => 'password'
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusEntity',
            'collection_class' => 'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusCollection',
            'service_name' => 'ShipmentStatus',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Clex\\V1\\Rest\\Merchant\\Controller' => 'HalJson',
            'Clex\\V1\\Rest\\Shipment\\Controller' => 'HalJson',
            'Clex\\V1\\Rest\\ShipmentStatus\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Clex\\V1\\Rest\\Merchant\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Clex\\V1\\Rest\\Shipment\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Clex\\V1\\Rest\\ShipmentStatus\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Clex\\V1\\Rest\\Merchant\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/json',
            ),
            'Clex\\V1\\Rest\\Shipment\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/json',
            ),
            'Clex\\V1\\Rest\\ShipmentStatus\\Controller' => array(
                0 => 'application/vnd.clex.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Clex\\V1\\Rest\\Merchant\\MerchantEntity' => array(
                'entity_identifier_name' => 'merchantId',
                'route_name' => 'clex.rest.merchant',
                'route_identifier_name' => 'merchant_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Clex\\V1\\Rest\\Merchant\\MerchantCollection' => array(
                'entity_identifier_name' => 'merchantId',
                'route_name' => 'clex.rest.merchant',
                'route_identifier_name' => 'merchant_id',
                'is_collection' => true,
            ),
            'Clex\\V1\\Rest\\Shipment\\ShipmentEntity' => array(
                'entity_identifier_name' => 'shipmentId',
                'route_name' => 'clex.rest.shipment',
                'route_identifier_name' => 'shipment_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Clex\\V1\\Rest\\Shipment\\ShipmentCollection' => array(
                'entity_identifier_name' => 'shipmentId',
                'route_name' => 'clex.rest.shipment',
                'route_identifier_name' => 'shipment_id',
                'is_collection' => true,
            ),
            'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusEntity' => array(
                'entity_identifier_name' => 'shipmentCode',
                'route_name' => 'clex.rest.shipment-status',
                'route_identifier_name' => 'shipment_status_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Clex\\V1\\Rest\\ShipmentStatus\\ShipmentStatusCollection' => array(
                'entity_identifier_name' => 'shipmentCode',
                'route_name' => 'clex.rest.shipment-status',
                'route_identifier_name' => 'shipment_status_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Clex\\V1\\Rest\\Merchant\\Controller' => array(
            'input_filter' => 'Clex\\V1\\Rest\\Merchant\\Validator',
        ),
        'Clex\\V1\\Rest\\ShipmentStatus\\Controller' => array(
            'input_filter' => 'Clex\\V1\\Rest\\ShipmentStatus\\Validator',
        ),
        'Clex\\V1\\Rest\\Shipment\\Controller' => array(
            'input_filter' => 'Clex\\V1\\Rest\\Shipment\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Clex\\V1\\Rest\\Merchant\\Validator' => array(
            0 => array(
                'name' => 'merchantName',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => '',
                'error_message' => 'merchantName field is required!',
            ),
            1 => array(
                'name' => 'companyName',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => '',
                'error_message' => 'companyName field is required!',
            ),
            2 => array(
                'name' => 'district',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'district field is required!',
            ),
            3 => array(
                'name' => 'street',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'street field is required!',
            ),
            4 => array(
                'name' => 'city',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'city field is required!',
            ),
            5 => array(
                'name' => 'zip',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'contact field is required!',
            ),
            6 => array(
                'name' => 'contact',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'contact field is required!',
            ),
            7 => array(
                'name' => 'telephone',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'telephone field is required!',
            ),
            8 => array(
                'name' => 'mobile',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'mobile field is required!',
            ),
            9 => array(
                'name' => 'email',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'email field is required!',
            ),
            10 => array(
                'name' => 'mainCompanyName',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'mainCompanyName field is required!',
            ),
        ),
        'Clex\\V1\\Rest\\ShipmentStatus\\Validator' => array(
            0 => array(
                'name' => 'shipmentCode',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'shipmentCode field is required!',
            ),
            1 => array(
                'name' => 'shipmentStatus',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'shipmentStatus field is required!',
            ),
        ),
        'Clex\\V1\\Rest\\Shipment\\Validator' => array(
            0 => array(
                'name' => 'merchantId',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'merchantId field is required!',
            ),
            1 => array(
                'name' => 'consigneeName',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeName field is required!',
            ),
            2 => array(
                'name' => 'consigneeStreet',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeStreet field is required!',
            ),
            3 => array(
                'name' => 'consigneeDistrict',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeDistrict field is required!',
            ),
            4 => array(
                'name' => 'consigneeAddress',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeAddress field is required!',
            ),
            5 => array(
                'name' => 'consigneeCity',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeCity field is required!',
            ),
            6 => array(
                'name' => 'consigneeTelephone',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeTelephone field is required!',
            ),
            7 => array(
                'name' => 'consigneeMobile',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'consigneeMobile field is required!',
            ),
            8 => array(
                'name' => 'goodsDescription',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'error_message' => 'goodsDescription field is required!',
            ),
            9 => array(
                'name' => 'merchantLatitude',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            10 => array(
                'name' => 'merchantLongitude',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            11 => array(
                'name' => 'merchantLocationName',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            12 => array(
                'name' => 'merchantLocationCity',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            13 => array(
                'name' => 'merchantLocationAddress',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            14 => array(
                'name' => 'merchantLocationMobile',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            15 => array(
                'name' => 'orderPrice',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
            16 => array(
                'name' => 'paymentMethod',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'error_message' => '',
            ),
        ),
    ),
);

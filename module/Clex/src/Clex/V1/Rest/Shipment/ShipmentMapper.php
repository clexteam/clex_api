<?php
/**
 * Created by PhpStorm.
 * User: Remon Adel
 * Date: 1/20/2017
 * Time: 12:21 AM
 */

namespace Clex\V1\Rest\Shipment;

use Herrera\Json\Exception\Exception;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;

class ShipmentMapper
{
    protected $adapter;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create($data)
    {
        isset($data->consigneeNo) ? $consigneeNo = $data->consigneeNo : $consigneeNo = "";
        isset($data->thirdPartyCode) ? $thirdPartyCode = $data->thirdPartyCode : $thirdPartyCode = 0;
        isset($data->consigneeZip) ? $consigneeZip = $data->consigneeZip : $consigneeZip = "";
        isset($data->merchantLatitude) ? $merchantLatitude = $data->merchantLatitude : $merchantLatitude = "";
        isset($data->merchantLongitude) ? $merchantLongitude = $data->merchantLongitude : $merchantLongitude = "";
        isset($data->merchantLocationCity) ? $merchantLocationCity = $data->merchantLocationCity : $merchantLocationCity = "";
        isset($data->merchantLocationName) ? $merchantLocationName = $data->merchantLocationName : $merchantLocationName = "";
        isset($data->merchantLocationAddress) ? $merchantLocationAddress = $data->merchantLocationAddress : $merchantLocationAddress = "";
        isset($data->merchantLocationMobile) ? $merchantLocationMobile = $data->merchantLocationMobile : $merchantLocationMobile = "";
        isset($data->orderPrice) ? $orderPrice = $data->orderPrice : $orderPrice = "";
        isset($data->paymentMethod) ? $paymentMethod = $data->paymentMethod : $paymentMethod = "";
        isset($data->itemsNumber) ? $itemsNo = $data->itemsNumber : $itemsNo = 1;
        isset($data->serviceType) ? $serviceType = $data->itemsNumber : $serviceType = "D";

        $sql = "INSERT INTO CL_SHIPMENT (CONS_NO, third_party_code, CUST_ACC, Branch_ID, CONS_NAME, CONS_DISTRICT, CONS_STREET, CONS_ADDRESS, CONS_CITY, CONS_ZIP,
              CONS_TEL, CONS_MOB, ITEMS_NUMBER, GOODS_DESC, CURR_DATE, CURR_USER, SHIPMENT_STATUS, merchant_latitude, merchant_longitude, merchant_location_name,
              merchant_location_city, merchant_location_address, merchant_location_mobile, order_price, SERVICE_TYPE, PAYMENT_METHOD)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            $resultset = $this->adapter->query($sql, array($consigneeNo, $thirdPartyCode, $data->customerAccount, $data->merchantId, $data->consigneeName, $data->consigneeDistrict,
                $data->consigneeStreet, $data->consigneeAddress, $data->consigneeCity, $consigneeZip, $data->consigneeTelephone, $data->consigneeMobile, $itemsNo, $data->goodsDescription, @date("Y-m-d:H-i-s")
            , $data->userId, 1, $merchantLatitude, $merchantLongitude, $merchantLocationName, $merchantLocationCity, $merchantLocationAddress, $merchantLocationMobile, $orderPrice, $serviceType, $paymentMethod));
        }catch (Exception $e){
            echo $e->getMessage();
        }

        $insertedId = $this->adapter->driver->getLastGeneratedValue();

        $sql = 'SELECT * FROM CL_SHIPMENT WHERE SIHPMENT_CODE = ? ORDER BY SIHPMENT_CODE DESC';
        $resultset = $this->adapter->query($sql, array($insertedId));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
        $entity = new ShipmentEntity();
        $entity->exchangeArray($data[0]);

        return $entity;
    }

    public function fetch($id)
    {
        try{
            $sql = 'SELECT * FROM CL_SHIPMENT WHERE SIHPMENT_CODE = ? OR third_party_code = ? ORDER BY SIHPMENT_CODE DESC';
            $resultset = $this->adapter->query($sql, array($id, $id));
            $data = $resultset->toArray();
            if (!$data) {
                return false;
            }
            $entity = new ShipmentEntity();
            $entity->exchangeArray($data[0]);
            return $entity;
        }catch (Exception $e){
            echo $e->getMessage();
        }

    }


    public function authenticate($id, $password)
    {
        if ($id != null && $password != null)
        {
            $sql = 'SELECT * FROM SEC_USER WHERE Branch_ID = ? AND USER_PWD = ?';
            $resultset = $this->adapter->query($sql,array($id, $password));
            $data = $resultset->toArray();
            if (!$data)
            {
                throw new Exception('unauthorized');
            }
            else
            {
                return $data;
            }
        }
        else
        {
            throw new Exception('unauthorized');
        }

    }

}

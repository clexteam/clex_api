<?php
namespace Clex\V1\Rest\Merchant;

class MerchantEntity
{
    public $id;
    public $custAcc;
    public $company;
    public $district;
    public $street;
    public $city;
    public $zip;
    public $contact;
    public $tel;
    public $mobile;
    public $email;
    public $custAccMain;
    public $password;

    public function getArrayCopy()
    {
        return array(
            'merchantId' => $this->id,
            'custAcc' => $this->custAcc,
            'company'  => $this->company,
            'district' => $this->district,
            'street'  => $this->street,
            'city'     => $this->city,
            'zip' => $this->zip,
            'contact'  => $this->contact,
            'tel'     => $this->tel,
            'mobile' => $this->mobile,
            'email'  => $this->email,
            'custAccMain'  => $this->custAccMain,
            'password'  => $this->password
        );
    }

    public function exchangeArray(array $array)
    {
        $this->id = $array['Branch_ID'];
        $this->custAcc = $array['CUST_ACC'];
        $this->company  = $array['SH_COMPANY'];
        $this->district  = $array['SH_DISTRICT'];
        $this->street  = $array['STREET'];
        $this->city  = $array['SH_CITY'];
        $this->zip  = $array['SH_ZIP'];
        $this->contact  = $array['SH_CONTACT'];
        $this->tel  = $array['SH_TEL'];
        $this->mobile  = $array['SH_MOB'];
        $this->email  = $array['email'];
        $this->custAccMain  = $array['CUST_ACC_MAIN'];
        $this->password  = $array['password'];
    }
}

<?php
namespace Clex\V1\Rest\Shipment;

class ShipmentEntity
{
    public $id;
    public $thirdPartyCode;
    public $consNo;
    public $consNoType;
    public $custAcc;
    public $merchantId;
    public $consName;
    public $consDistrict;
    public $consStreet;
    public $consAddress;
    public $consCity;
    public $consZip;
    public $consContact;
    public $consTel;
    public $consMob;
    public $packageType;
    public $itemsNumber;
    public $goodsDesc;
    public $serviceType;
    public $paymentMethod;
    public $dangerousGood;
    public $isPosted;
    public $currDate;
    public $currUser;
    public $notes;
    public $shipmentStatus;
    public $statusNotes;
    public $ipAddress;
    public $receivedDate;
    public $isChargeable;
    public $rate;
    public $clexNote;
    public $latitude;
    public $longitude;
    public $merchantLatitude;
    public $merchantLongitude;
    public $merchantLocationName;
    public $merchantLocationCity;
    public $merchantLocationAddress;
    public $merchantLocationMobile;
    public $orderPrice;
    public $printUrl;

    public function getArrayCopy()
    {
        return array(
            'shipmentId' => $this->id,
            'thirdPartyCode' => $this->thirdPartyCode,
            'consNo'  => $this->consNo,
            'consNoType' => $this->consNoType,
            'custAcc'  => $this->custAcc,
            'merchantId'     => $this->merchantId,
            'consName' => $this->consName,
            'consDistrict'  => $this->consDistrict,
            'consStreet'     => $this->consStreet,
            'consAddress' => $this->consAddress,
            'consCity'  => $this->consCity,
            'consZip'  => $this->consZip,
            'consContact'  => $this->consContact,
            'consTel'  => $this->consTel,
            'consMob'  => $this->consMob,
            'packageType'  => $this->packageType,
            'itemsNumber'  => $this->itemsNumber,
            'goodsDesc'  => $this->goodsDesc,
            'serviceType'  => $this->serviceType,
            'paymentMethod'  => $this->paymentMethod,
            'dangerousGood'  => $this->dangerousGood,
            'isPosted'  => $this->isPosted,
            'currDate'  => $this->currDate,
            'currUser'  => $this->currUser,
            'notes'  => $this->notes,
            'shipmentStatus'  => $this->shipmentStatus,
            'statusNotes'  => $this->statusNotes,
            'ipAddress'  => $this->ipAddress,
            'receivedDate'  => $this->receivedDate,
            'isChargeable'  => $this->isChargeable,
            'rate'  => $this->rate,
            'clexNote'  => $this->clexNote,
            'latitude'  => $this->latitude,
            'longitude'  => $this->longitude,
            'merchantLatitude'  => $this->merchantLatitude,
            'merchantLongitude'  => $this->merchantLongitude,
            'merchantLocationCity'  => $this->merchantLocationCity,
            'merchantLocationName'  => $this->merchantLocationName,
            'merchantLocationAddress'  => $this->merchantLocationAddress,
            'merchantLocationMobile'  => $this->merchantLocationMobile,
            'orderPrice'  => $this->orderPrice,
            'printUrl'  => $this->printUrl,

        );
    }

    public function exchangeArray(array $array)
    {
        $this->id = $array['SIHPMENT_CODE'];
        $this->thirdPartyCode = $array['third_party_code'];
        $this->consNo  = $array['CONS_NO'];
        $this->consNoType  = $array['CONS_NO_TYPE'];
        $this->custAcc  = $array['CUST_ACC'];
        $this->merchantId  = $array['Branch_ID'];
        $this->consName  = $array['CONS_NAME'];
        $this->consDistrict  = $array['CONS_DISTRICT'];
        $this->consStreet  = $array['CONS_STREET'];
        $this->consAddress  = $array['CONS_ADDRESS'];
        $this->consCity  = $array['CONS_CITY'];
        $this->consZip  = $array['CONS_ZIP'];
        $this->consContact  = $array['CONS_CONTACT'];
        $this->consTel  = $array['CONS_TEL'];
        $this->consMob  = $array['CONS_MOB'];
        $this->packageType  = $array['PACKAGE_TYPE'];
        $this->itemsNumber  = $array['ITEMS_NUMBER'];
        $this->goodsDesc  = $array['GOODS_DESC'];
        $this->serviceType  = $array['SERVICE_TYPE'];
        $this->paymentMethod  = $array['PAYMENT_METHOD'];
        $this->dangerousGood  = $array['DANGEROUS_GOODS'];
        $this->isPosted  = $array['IS_POSTED'];
        $this->currDate  = $array['CURR_DATE'];
        $this->currUser  = $array['CURR_USER'];
        $this->notes  = $array['NOTES'];
        $this->shipmentStatus  = $array['SHIPMENT_STATUS'];
        $this->statusNotes  = $array['STATUS_NOTES'];
        $this->ipAddress  = $array['IP_ADDRESS'];
        $this->receivedDate  = $array['RECEIVED_DATE'];
        $this->isChargeable  = $array['IS_CHARGEABLE'];
        $this->rate  = $array['RATE'];
        $this->clexNote  = $array['CLEX_NOTE'];
        $this->latitude  = $array['latitude'];
        $this->longitude  = $array['longitude'];
        $this->merchantLatitude = $array['merchant_latitude'];
        $this->merchantLongitude = $array['merchant_longitude'];
        $this->merchantLocationCity = $array['merchant_location_city'];
        $this->merchantLocationName = $array['merchant_location_name'];
        $this->merchantLocationAddress = $array['merchant_location_address'];
        $this->merchantLocationMobile = $array['merchant_location_mobile'];
        $this->orderPrice = $array['order_price'];
        $this->printUrl = "http://www.clexonline.com/Shipment/ApiPdf.php?id=" . $array['SIHPMENT_CODE'];

    }
}

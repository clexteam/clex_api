<?php
namespace Clex\V1\Rest\ShipmentStatus;

class ShipmentStatusResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Clex\V1\Rest\ShipmentStatus\ShipmentStatusMapper');
        return new ShipmentStatusResource($mapper);
    }
}

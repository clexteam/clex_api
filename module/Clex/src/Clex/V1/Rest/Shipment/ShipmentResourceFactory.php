<?php
namespace Clex\V1\Rest\Shipment;

class ShipmentResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Clex\V1\Rest\Shipment\ShipmentMapper');
        return new ShipmentResource($mapper);
    }
}

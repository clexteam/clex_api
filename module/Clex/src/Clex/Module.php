<?php
namespace Clex;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Clex\V1\Rest\Merchant\MerchantMapper' =>  function ($sm) {
                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new \Clex\V1\Rest\Merchant\MerchantMapper($adapter);
                },
                'Clex\V1\Rest\ShipmentStatus\ShipmentStatusMapper' =>  function ($sm) {
                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new \Clex\V1\Rest\ShipmentStatus\ShipmentStatusMapper($adapter);
                },
                'Clex\V1\Rest\Shipment\ShipmentMapper' =>  function ($sm) {
                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new \Clex\V1\Rest\Shipment\ShipmentMapper($adapter);
                },
            ),
        );
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Remon Adel
 * Date: 1/20/2017
 * Time: 12:21 AM
 */

namespace Clex\V1\Rest\ShipmentStatus;

use Herrera\Json\Exception\Exception;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;

class ShipmentStatusMapper
{
    protected $adapter;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function create($data)
    {
        if (isset($data->notes))
        {
            $notes = $data->notes;
        }
        else
        {
            $notes = "";
        }
        $sql = "UPDATE CL_SHIPMENT SET SHIPMENT_STATUS = ?, STATUS_NOTES = ? WHERE SIHPMENT_CODE = ?";
        try{
            $this->adapter->query($sql, array($data->shipmentStatus, $data->notes, $data->shipmentCode));
        }catch (Exception $e){
            echo $e->getMessage();
        }

        $hydratedData = array('SIHPMENT_CODE' => $data->shipmentCode, 'STATUS_NOTES' => $data->notes, 'SHIPMENT_STATUS' => $data->shipmentStatus, 'title' => "Cancelled", 'details' => "Printed Wrong & its Cancelled" );

        $entity = new ShipmentStatusEntity();
        $entity->exchangeArray($hydratedData);

        return $entity;
    }

    public function fetchAll($params = [])
    {
        if ($params['merchant_id'])
        {
            $select = new Select('CL_SHIPMENT');
            $select->columns(array('SIHPMENT_CODE', 'SHIPMENT_STATUS', 'STATUS_NOTES'));
            $select->join('Status','CL_SHIPMENT.SHIPMENT_STATUS = Status.id');
            $select->where("Branch_ID = '" . $params['merchant_id'] . "'");
            $select->order('SIHPMENT_CODE desc');
            $paginatorAdapter = new DbSelect($select, $this->adapter);
            $collection = new ShipmentStatusCollection($paginatorAdapter);
            return $collection;
        }
        else
        {
            return false;
        }

    }

    public function fetch($id)
    {
        $sql = 'SELECT CL_SHIPMENT.SIHPMENT_CODE, CL_SHIPMENT.SHIPMENT_STATUS, CL_SHIPMENT.STATUS_NOTES,
              Status.title, Status.details FROM CL_SHIPMENT JOIN Status ON CL_SHIPMENT.SHIPMENT_STATUS = Status.id WHERE SIHPMENT_CODE = ?';
        $resultset = $this->adapter->query($sql, array($id));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

        $entity = new ShipmentStatusEntity();
        $entity->exchangeArray($data[0]);

        return $entity;
    }

    public function authenticate($id, $password)
    {
        if ($id != null && $password != null)
        {
            $sql = 'SELECT * FROM SEC_USER WHERE Branch_ID = ? AND USER_PWD = ?';
            $resultset = $this->adapter->query($sql,array($id, $password));
            $data = $resultset->toArray();
            if (!$data)
            {
                throw new Exception('unauthorized');
            }
            else
            {
                return 1;
            }
        }
        else
        {
            throw new Exception('unauthorized');
        }

    }

}
